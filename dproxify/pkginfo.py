# dproxify package info
# Written by Francesco Palumbo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


DESCRIPTION = 'An object daemon proxifier for GNU/Linux (Python 3.x)'

CLASSIFIERS = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: Python :: 3.4',
]

MODNAME = 'dproxify'
VERSION = '0.1.2-r1'
LICENSE = 'GNU General Public License v3'
AUTHOR  = 'Francesco Palumbo'
EMAIL   = 'phranz.dev@gmail.com'
REPO    = 'https://codeberg.org/phranz/dproxify'
